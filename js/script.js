const main_header = {
    img: "images/neiu_main_logo.png",
    title: "AGUA en COMUNIDADES EXPERIMENTALES",
    tagLine: "Water in Research Communities"
}

const main_side = {
    list:[
            {
                text: "About the Program",
                id: "about-the-prog",
            },
            {
                text: "People",
                members:[{
                            name: "Pam Geddes",
                            id: "about-p-geddes",
                            selected: ""
                        },
                        {
                            name: "Ken Voglesonger",
                            id: "about-k-voglesonger",
                            selected: ""
                        },
                        {
                            name: "Shannon Saszik",
                            id: "about-s-saszik",
                            selected: ""
                        },
                        {
                            name: "Laura Sanders",
                            id: "about-l-sanders",
                            selected: "false"
                        },
                        {
                            name: "Maureen Erber",
                            id: "about-m-erber",
                            selected: "false"
                        },
                        {
                            name: "Jean Hemzacek",
                            id: "about-j-hemzacek",
                            selected: "false"
                        }            
                        ]   
            },
            {
                text: "Apply Now",
                link: "https://docs.google.com/forms/d/1Q__QCbhvOollefCGcI9UWrhIjOECa2ycUYwt1M0o99I/edit"
            }
        ],
    contact: {
        text: "Contact Us:",
        contact_name: "Dr. Pam Geddes",
        contact_email: "ace@neiu.edu"
    },
    sponsor:{
        img: "images/nsf-logo.png",
        link: "https://nsf.gov"
    }
}




const main_footer = {
    pre_footer: "Funded by National Science Foundation DUE Hispanic-Serving Institutions Program Award 1832421.",
    footer:{
        links:[
            {
                name: "Links to NEIU home" ,
                link: ""
            },
            {
                name: "ACE Program Home" ,
                link: ""
            },
            {
                name: "Learning Success Center" ,
                link: ""
            },
            {
                name: "Admissions Office" ,
                link: ""
                    },
            {
                name: "Financial Aid" ,
                link: ""
            },
            {
                name: "Student Disability Services" ,
                link: ""
            },
            {
                name: "Scholarship Office" ,
                link: ""
            },
            {
                name: "Counseling Office" ,
                link: ""
            },
            {
                name: "Career Development" ,
                link: ""
            },
            {
                name: "Angelina Pedroso Center for Diversity and Intercultural Affairs",
                link: ""
            },
            {
                name: "Student Center for Science Engagement",
                link: ""
            }
        ],
        contact:{
            title: "Northeastern Illinois University",
            address: ["5500 North St. Louis Avenue","Chicago, Illinois 60625-4699","(773) 583-4050"],
            date:"2019 Northeastern Illinois University"
			// © . | <a href="https://www.neiu.edu/legal">Legal</a>
        }
    }
}

var listMembersElement = main_side.list[1].members.reduce((total, value, index, array,) =>
 total + `<li class="nav-item ${value['selected']}"><a class="nav-link" role="tab" data-toggle="tab" href="#${value['id']}">${value['name']}</a></li>`,'' );
console.log("check this out:  "+listMembersElement);

// -------------------------------------------------- SIDE BAR
document.querySelector("#side-bar").innerHTML = `
<ul class="">
    <li> <a href=""> ${main_side.list[0].text}</a></li>
    <li>${main_side.list[1].text}
        <ul class= "nav nav-tabs flex-column" >${listMembersElement}</ul>
    </li>
</ul>
    <a class="btn apply-now" href="${main_side.list[2].link}" target="_blank">${main_side.list[2].text}</a>

<div class="side-bar-footer"> <!-- small-side-footer -->
    <span>Contact us:<br/>
    Dr. Pam Geddes<br/>
    ace@neiu.edu</span>
    <a href="${main_side.sponsor.link}" target="_blank"> <img class="img-fluid text-center" src="${main_side.sponsor.img}" alt="National Science Foundation Logo" /> </a>
</div>`;



document.querySelector("#footer-links").innerHTML = main_footer.footer.links.reduce((total,item, index, arr) =>
  total + "<span title=\"mari says: need the links\"><a href=\""+item['link']+"\">"+item['name']+"</a></span>"+ ((index== arr.length-1)? "" : " | "),'');


var f = document.querySelector("#footer-contact");

f.innerHTML += `<div class="col-md-3 footer-logo"><a href="/"><img class="img-fluid" src="${main_header['img']}" alt="Northeastern Illinois University"></a></div>
    <div class="col-md-6 contact-info">
    <div class="" ><p> 
    <span style="color: #0036a4">
    ${main_footer.footer.contact['title']}</span><br/>
    ${main_footer.footer.contact['address'][0]}<br/>
    ${main_footer.footer.contact['address'][1]}<br/>
    ${main_footer.footer.contact['address'][2]}<br/>
    ${main_footer.footer.contact['date']}</p></div>
        '© 2019 Northeastern Illinois University. | <a href="https://www.neiu.edu/legal">Legal</a> 
                    </p>
    </div>
    <div class="col-md-3 social">
        <a class="linkedin" href="https://www.neiu.edu/linkedin">LinkedIn</a>
        <a class="youtube" href="https://www.neiu.edu/youtube">Youtube</a>
        <a class="instagram" href="https://www.instagram.com/neiulife/">Instagram</a>
        <a class="twitter" href="https://www.neiu.edu/twitter">Twitter</a>
        <a class="facebook" href="https://www.neiu.edu/facebook">Facebook</a>
        <div style="clear: both;"></div>
    </div>
    <div style="clear: both;"></div>`;

// f.children[0].innerHTML = "first stuff";
// f.children[1].innerHTML = "second stuff";
